//
//  ScrollContentView.h
//  LDJR
//
//  Created by 郭正茂 on 15/10/26.
//  Copyright © 2015年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollContentView : UIView
@property(nonatomic,strong)NSLayoutConstraint *heightConstraint;
@property(nonatomic,strong)NSLayoutConstraint *widthConstraint;
@end
