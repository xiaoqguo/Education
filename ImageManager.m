//  ImageManager.m
//  GMSupperWH
//
//  Created by apple on 14-10-24.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import "ImageManager.h"
@implementation ImageManager


+(CGSize)getImageSizeWithWidth:(NSInteger)width withImageSize:(CGSize)imgSize{
    float x=imgSize.height/imgSize.width;
    return CGSizeMake(width, width*x);
}
+(CGRect)getFrameWithImageSize:(CGSize)imgSize withContentSize:(CGSize)contentSize{
    float x=0,y=0,width,heigh;
    float wScale=imgSize.width/contentSize.width;
    float hScale=imgSize.height/contentSize.height;
    if (imgSize.width<contentSize.width&&imgSize.height<contentSize.height) {
        width=imgSize.width;
        heigh=imgSize.height;
        y=(contentSize.height-heigh)/2;
        x=(contentSize.width-width)/2;
    }else{
    float scale=1.0;
    if (wScale>hScale) {
        scale=wScale;
        width=imgSize.width/scale;
        heigh=imgSize.height/scale;
        y=(contentSize.height-heigh)/2;
    }else {
        scale=hScale;
        width=imgSize.width/scale;
        heigh=imgSize.height/scale;
        x=(contentSize.width-width)/2;
    }
    }
    return CGRectMake(x, y, width, heigh);
}



//默认压缩尺寸
+(UIImage*)compressImage:(UIImage*)img{
    CGSize size=[ImageManager getImageSizeWithWidth:320 withImageSize:img.size];
    return [ImageManager compressImage:img forSize:size];
}
//将图片压缩到尺寸
+(UIImage*)compressImage:(UIImage*)img forSize:(CGSize)size{
    UIImage *newImage = nil;
    // 绘制图片
    UIGraphicsBeginImageContext(size);//this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width = size.width;
    thumbnailRect.size.height = size.height;
    [img drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //绘制图片结束
    return newImage;
}
+(NSArray*)getNameAndTypeWithFileName:(NSString*)fileName{
    NSArray *array=[fileName componentsSeparatedByString:@"."];
    if ([array count]<1) {
        return nil;
    }else if([array count]==2){
        return array;
    }else{
        NSString *type=[array objectAtIndex:([array count]-1)];
        NSString *name=[array objectAtIndex:0];
        for(int i=1;i<[array count]-1;i++){
            NSString *str=[array objectAtIndex:i];
            name=[NSString stringWithFormat:@"%@.%@",name,str];
        }
        return [NSArray arrayWithObjects:name,type,nil];
    }
}

@end
