//
//  GZTabScrollView.h
//  GZGroup
//
//  Created by guozhengmao on 15/9/17.
//  Copyright (c) 2015年 guozhengmao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GZTabScrollView : UIView<UIScrollViewDelegate>{
    int _with;//视图宽度
    NSInteger _pageCount;//页数
    int _cellWidth;
    int _tabBarType;//0 top 1 bottom 默认0
    int _cusorType;//1 top 0 bottom 默认0
    int _tabBarHeight;//TabBar的高度 默认40
    NSInteger _pageIndex;//正在显示的页码
    int _pageControlY;//pageControl的高度，默认是底部 如果是-
    UIColor *_tabItemNormalColor;//默认灰色
    UIColor *_tabItemSelectedColor;//默认白色
    UIView *_cusorView;
    UIView *_cusorLineView;//光标的背景
    UIColor *_cusorColor;//默认红色
    UIColor *_cusorLineColor;//默认灰色
    UIView *_tabBarView;
    NSMutableArray *_subViewArray;//scrollView上的子视图列表
    NSMutableArray *_tabBarItemArray;
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;
    UIButton *_leftBT;
    UIButton *_rightBT;
    UIView *_selectedTabBarItem;//高亮的tabBarItem
    
    int _tabScrollViewType;//0 lable type 1 image type 2 view type
    BOOL _userTouchScrollView;
}
//各种事件 block!!
@property(nonatomic,copy) void (^didSelectTabBarItemBlock)(GZTabScrollView* tabScrollView,NSInteger pageIndex);
//设置属性
-(void)setTabItemNormalColor:(UIColor*)color;
-(void)setTabItemSelectedColor:(UIColor*)color;
-(void)setCusorColor:(UIColor*)cusorColor withCusorLineColor:(UIColor*)lineColor;
-(void)scrollToPage:(NSInteger)pageIndex;
-(void)setTabBarHeight:(int)height;
-(CGSize)getSubViewSize;
-(CGSize)getTabBarItemSizeWithPageCount:(NSInteger)pageCount;
-(void)setCusorType:(int)type;
-(void)setPageControlLightColor:(UIColor*)color;
//build
-(void)buildWithTitleArray:(NSArray*)titleArray withSubViewArray:(NSArray*)subViewArray;
-(void)buildWithImageArray:(NSArray*)imgArray withSubViewArray:(NSArray*)subViewArray;
-(void)buildWithViewArray:(NSArray*)subViewArray withSubViewArray:(NSArray*)subViewArray;
//子控件操作
-(void)addButtons;
-(void)removeButtons;
-(void)setPageControlY:(int)y;//
-(void)addPageControl;
-(void)removePageControl;

@end
