//
//  LoginViewController.h
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property(nonatomic,copy)void (^studentLoginBlock)();
@property(nonatomic,copy)void (^teacherLoginBlock)();
@end
