//
//  RLDrawViewController.m
//  Education
//
//  Created by 郭正茂 on 2016/11/2.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "RLDrawViewController.h"
#import "UIView+MyAutoLayout.h"
@interface RLDrawViewController ()

@property(nonatomic,strong)NSLayoutConstraint *mainLeftCons;
@property(nonatomic,strong)NSLayoutConstraint *mainRightCons;
@property(nonatomic,strong)NSLayoutConstraint *leftWidthCons;
@property(nonatomic,strong)NSLayoutConstraint *rightWidthCons;
@property(nonatomic,strong)NSLayoutConstraint *leftLeftCons;
@property(nonatomic,strong)NSLayoutConstraint *rightRightCons;
@property(nonatomic,assign)CGFloat leftWidth;
@property(nonatomic,assign)CGFloat rightWidth;
@property(nonatomic,assign)BOOL showingLeft;
@property(nonatomic,assign)BOOL showingRight;
@end

@implementation RLDrawViewController
-(id)init{
    self=[super init];
    if (self) {
        self.showingLeft=NO;
        self.showingRight=NO;
        self.mainView=[[UIView alloc] initWithFrame:CGRectZero];
        self.mainView.translatesAutoresizingMaskIntoConstraints=NO;
        [self.view addSubview:self.mainView];
        
        self.view.clipsToBounds=YES;
        
        [self.mainView addConstraint:[self.mainView getWidthConstraintWithWidth:[UIView getScreenWidth]]];
        [self.view addVerticalConstraintSubView:self.mainView];
        self.mainLeftCons=[self.view getLeftConstraint:0 withSubView:self.mainView];
        [self.view addConstraint:self.mainLeftCons];
    }
    return self;
}

-(void)initCoverView{
    if(self.coverView==nil){
        self.coverView=[[UIView alloc] initWithFrame:CGRectZero];
        self.coverView.translatesAutoresizingMaskIntoConstraints=NO;
        [self.mainView addSubview:self.coverView];
        
        [self.mainView filledWith:self.coverView];
        [self.coverView setBackgroundColor:[UIColor blackColor]];
        self.coverView.alpha=0;
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissDrawAction)];
        [self.coverView addGestureRecognizer:tap];
    }
}
-(void)dissDrawAction{
    if(self.showingLeft){
        [self dissLeftDrawView];
    }
    if(self.showingRight){
        [self dissRightDrawView];
    }
}
-(void)setLeftViewWidth:(CGFloat)width{
    self.leftWidth=width;
    if(self.leftWidthCons==nil){
        self.leftView=[[UIView alloc] initWithFrame:CGRectZero];
        self.leftView.translatesAutoresizingMaskIntoConstraints=NO;
        [self.view addSubview:self.leftView];
        [self.view sendSubviewToBack:self.leftView];
        
        self.leftLeftCons=[self.view getLeftConstraint:-width withSubView:self.leftView];
        [self.view addConstraint:self.leftLeftCons];
        self.leftWidthCons=[self.leftView getWidthConstraintWithWidth:width];
        [self.leftView addConstraint:self.leftWidthCons];
        [self.view addVerticalConstraintSubView:self.leftView];

    }else{
        self.leftWidthCons.constant=width;
        if (self.showingLeft) {
            self.mainLeftCons.constant=width;
        }
    }
}
-(void)setRightViewWidth:(CGFloat)width{
    self.rightWidth=width;
    if(self.mainRightCons==nil){
        self.rightView=[[UIView alloc] initWithFrame:CGRectZero];
        self.rightView.translatesAutoresizingMaskIntoConstraints=NO;
        [self.view addSubview:self.rightView];
        [self.view sendSubviewToBack:self.rightView];
        
        self.rightRightCons=[self.view getRightContraint:-width withSubView:self.rightView];
        [self.view addConstraint:self.rightRightCons];
        self.rightWidthCons=[self.rightView getWidthConstraintWithWidth:width];
        [self.rightView addConstraint:self.rightWidthCons];
        [self.view addVerticalConstraintSubView:self.rightView];
    }else{
        self.rightWidthCons.constant=width;
        if(self.showingRight){
            self.mainRightCons.constant=width;
        }
    }
}
-(void)showLeftDrawView{
    if(self.leftView!=nil){
        [self initCoverView];
        self.showingLeft=true;
        [self performSelector:@selector(leftDrawAnimation) withObject:nil afterDelay:0.1];
    }
}
-(void)leftDrawAnimation{
    [UIView animateWithDuration:0.4 animations:^{
        self.leftLeftCons.constant=0;
        self.mainLeftCons.constant=self.leftWidth;
        self.coverView.alpha=0.7;
        [self.view layoutIfNeeded];
    }];
}
-(void)showRightDrawView{
    if(self.rightView!=nil){
        [self initCoverView];
        self.showingRight=true;
        [self performSelector:@selector(rightDrawAnimation) withObject:nil afterDelay:0.1];
    }
}
-(void)rightDrawAnimation{
    [UIView animateWithDuration:0.4 animations:^{
        self.rightRightCons.constant=0;
        self.mainLeftCons.constant=-self.rightWidth;
        self.coverView.alpha=0.7;
        [self.view layoutIfNeeded];
    }];
}
-(void)dissLeftDrawView{
    if(self.leftView!=nil){
        self.showingLeft=false;
        [UIView animateWithDuration:0.4 animations:^{
            self.leftLeftCons.constant=-self.leftWidth;
            self.mainLeftCons.constant=0;
            self.coverView.alpha=0.0;
            [self.view layoutIfNeeded];
        }];
    }
}
-(void)dissRightDrawView{
    if(self.rightView!=nil){
        self.showingRight=false;
        [UIView animateWithDuration:0.4 animations:^{
            self.rightRightCons.constant=self.rightWidth;
            self.mainLeftCons.constant=0;
            self.coverView.alpha=0.0;
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
