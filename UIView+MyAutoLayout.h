//
//  UIView+MyAutoLayout.h
//  LDJR
//
//  Created by 郭正茂 on 15/9/30.
//  Copyright © 2015年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+MyAutoLayout.h"

typedef NS_ENUM(NSInteger, GZAutolayoutLineType) {
    GZAutolayoutLineTypeTop,
    GZAutolayoutLineTypeleft,
    GZAutolayoutLineTypeRight,
    GZAutolayoutLineTypeBottom
};

typedef NS_ENUM(NSInteger,GZAutoLayoutVeriticalChaintype){
    GZAutolayoutVeriticalChainLeft,
    GZAutolayoutVeriticalChainRight,
    GZAutolayoutVeriticalChainCenter
};
@interface UIView (GZAutoLayout)

+(CGFloat)getScreenHeight;
+(CGFloat)getScreenWidth;
+(CGRect)getScreeFrame;
//绘制边线边线方法
//thick 线的厚度
//startDistance 和 endDistance描绘线与开始端与结束端的距离，开始结束的规则为 从左到右 从上倒下
//没有直接使用topDistance这样的字段是因为 横线没有上下，竖线没有左右，所有用start 和 end 
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType
                    withSideColor:(UIColor*)color
                        withThick:(CGFloat)thick
                withStartDistance:(NSInteger)sDistance
                  withEndDistance:(NSInteger)eDistance;
//两边边界宽度一样的时候调用
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType
                    withSideColor:(UIColor*)color
                        withThick:(CGFloat)thick withSideDistance:(NSInteger)sDistance;
//两边都没有距离时调用
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType
                    withSideColor:(UIColor*)color withThick:(CGFloat)thick;


//获得高度宽度的contraint
-(NSLayoutConstraint*)getHeightConstraintWithHeight:(CGFloat)height;
-(NSLayoutConstraint*)getWidthConstraintWithWidth:(CGFloat)width;
-(NSLayoutConstraint*)getWCompareHConstraintWithRatio:(CGFloat)ratio;


//子试图与self的关系
//子视图完全填充父视图
-(void)filledWith:(UIView*)subView;
-(void)filledWith:(UIView *)subView margin:(CGFloat)margin;
-(void)filledWith:(UIView *)subView horiMargin:(CGFloat)hm vertiMargin:(CGFloat)vm;
-(void)filledWith:(UIView *)subView topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm;

//添加长高固定的子视图constraint
-(void)addConstraintSubView:(UIView*)subView width:(CGFloat)width height:(CGFloat)height;
-(void)addConstraintWidth:(CGFloat)width height:(CGFloat)height;
//添加子试图与父视图水平竖直关系
-(void)addHorizontalConstraintSubView:(UIView*)subView leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin;
-(void)addVerticalConstraintSubView:(UIView*)subView topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin;
-(void)addHorizontalConstraintSubView:(UIView*)subView margin:(CGFloat)margin;
-(void)addVerticalConstraintSubView:(UIView*)subView margin:(CGFloat)margin;
-(void)addHorizontalContraintSubView:(UIView*)subView;
-(void)addVerticalConstraintSubView:(UIView*)subView;
//子试图 居中
-(NSLayoutConstraint*)getXCenterConstraintWithSubView:(UIView*)subView;
-(NSLayoutConstraint*)getYCenterConstrainWithSubView:(UIView*)subView;
-(void)addCenterConstrainWithSubView:(UIView*)subView;
-(NSLayoutConstraint*)getYCenterConstrainWithSubView:(UIView*)subView withConstant:(CGFloat)constant;
-(NSLayoutConstraint*)getXCenterConstraintWithSubView:(UIView*)subView withConstant:(CGFloat)constant;

//获得子试图上下左右距离基本contraint
-(NSLayoutConstraint*)getTopConstraint:(CGFloat)top withSubView:(UIView*)view;
-(NSLayoutConstraint*)getLeftConstraint:(CGFloat)left withSubView:(UIView*)view;
-(NSLayoutConstraint*)getRightContraint:(CGFloat)right withSubView:(UIView*)view;
-(NSLayoutConstraint*)getBottomContraint:(CGFloat)bottom withSubView:(UIView*)view;


//多子试图与self的关系
//等宽、等高 挤满 父视图
-(void)addHorizontalSubViews:(NSArray*)subViews leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm viewMargin:(CGFloat)vm;
-(void)addVerticalSubViews:(NSArray*)subViews topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm viewMargin:(CGFloat)vm;
//子视图成串，居左，右，中 间距相等
//注意不要忘记设置首view或者 尾view与父视图的边界距离
-(void)addHorizontalChainSubViews:(NSArray*)subViews chaintype:(GZAutoLayoutVeriticalChaintype)type constant:(CGFloat)constant margin:(CGFloat)margin;
-(void)addVerticalChainSubViews:(NSArray*)subViews chaintype:(GZAutoLayoutVeriticalChaintype)type constant:(CGFloat)constant margin:(CGFloat)margin;

//子试图与子试图的关系
//子试图顶底相接
+(NSLayoutConstraint*)getConsTopView:(UIView*)tv bottomView:(UIView*)bv margin:(CGFloat)margin;
//子试图首尾相接
+(NSLayoutConstraint*)getConsLeftView:(UIView*)lv rightView:(UIView*)rv margin:(CGFloat)margin;
//两视图宽度相等
+(NSLayoutConstraint*)getConsEqualWithAView:(UIView*)aView bView:(UIView*)bView withRadio:(CGFloat)radio;
//两视图高度相等
+(NSLayoutConstraint*)getConsEqualHeightAView:(UIView*)aView bView:(UIView*)bView withRadio:(CGFloat)radio;
//两视图某个基本布局参数相等
-(NSLayoutConstraint*)getWidthEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple;
-(NSLayoutConstraint*)getHeightEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple;

//两子视图 中心相等
-(NSLayoutConstraint*)getXCenterEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple;
-(NSLayoutConstraint*)getXCenterEqualConsToView:(UIView*)toView constant:(CGFloat)constant;
-(NSLayoutConstraint*)getXCenterEqualConsToView:(UIView*)toView;
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView constant:(CGFloat)constant;
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView;
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple;

//子视图位于另一视图正下方 另一子视图左对齐，且宽度相等 sview在下 uView在上
-(void)addContraintsSubView:(UIView*)sView underView:(UIView*)uView constant:(CGFloat)constant;
-(void)addContraintsSubView:(UIView*)sView underView:(UIView*)uView constant:(CGFloat)constant height:(CGFloat)height;

-(NSLayoutConstraint*)getTopEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant;
-(NSLayoutConstraint*)getBottomEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant;
-(NSLayoutConstraint*)getLeftEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant;
-(NSLayoutConstraint*)getRightEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant;
//子视图是另一子视图下方，且两者左对齐，宽相等
+(void)addContraintsSubView:(UIView*)sView underView:(UIView*)uView constant:(CGFloat)constant;


//自身与其他视图关系
//左侧对齐
//父视图方法 上下左右对齐
-(void)addConsSubView:(UIView*)sv sameLayoutToView:(UIView*)view;
-(void)addConsSubView:(UIView *)sv licklyLayoutToView:(UIView *)view topCon:(CGFloat)top bottomCon:(CGFloat)bottom leftCon:(CGFloat)left rightCon:(CGFloat)right;


//图片相关
-(UIImageView*)addHoriImgViewWithName:(NSString*)name;
-(UIImageView*)addCenterImageWithImage:(UIImage*)img sacle:(CGFloat)sacle;
-(UIImageView*)addImageConsWithImg:(UIImage*)img sacle:(CGFloat)sacle;

//图片按钮相关
-(UIButton*)addCenterBTWithImage:(UIImage*)omg sacle:(CGFloat)sacle;
-(UIButton*)addBTWithImage:(UIImage*)img sacle:(CGFloat)sacle;
//scrollView相关
//autolayout与ScorllView结合相当麻烦，若是有很多子界面的话更是容易混乱。
//这个界面作为众多子界面与scrollview的桥梁就可以避免这种混乱
//这个仅限于上下滑动的scorllview（目前项目中主要用到）其他类型的可以根据这个方法演变出来，有空余时间再写，
//返回self的高度constraint，当高度变化时，修改这个属性就可以了
-(NSLayoutConstraint*)addToScrollView:(UIScrollView*)view withHeight:(CGFloat)height;
-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withWidth:(CGFloat)width;




-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withHeightMultiplier:(CGFloat)multiplier;
-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withWidthMultiplier:(CGFloat)multiplier;
-(void)addToScrollView:(UIScrollView*)view withHeightMutiple:(CGFloat)mutiple;
-(void)addToScrollView:(UIScrollView*)view withWidthMutiple:(CGFloat)mutiple;

@end
