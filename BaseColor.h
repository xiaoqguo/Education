//
//  BaseColor.h
//  LDJR
//
//  Created by 郭正茂 on 15/9/16.
//  Copyright (c) 2015年 ldjt. All rights reserved.
//

#ifndef LDJR_BaseColor_h
#define LDJR_BaseColor_h

#define main_deep_color [UIColor colorWithRed:50.0/255.0 green:65.0/255.0 blue:76.0/255.0 alpha:1]
#define main_normal_color [UIColor colorWithRed:100.0/255.0 green:130.0/255.0 blue:160.0/255.0 alpha:1]
#define main_light_color [UIColor colorWithRed:200/255.0 green:210.0/255.0 blue:220.0/255.0 alpha:1]
#define text_primary_color [UIColor colorWithRed:33.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1]
#define text_secondary_color [UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:1]
#define divider_color [UIColor colorWithRed:189.0/255.0 green:189.0/255.0 blue:189.0/255.0 alpha:1]
#define white_color [UIColor colorWithRed:1 green:1 blue:1 alpha:1]
#define green_color [UIColor colorWithRed:0.0/255.0 green:150.0/255.0 blue:180.0/255.0 alpha:1]

#define success_color [UIColor colorWithRed:27.0/255.0 green:170.0/255.0 blue:235.0/255.0 alpha:1.0]
#define error_color [UIColor colorWithRed:251.0/255.0 green:47.0/255.0 blue:48.0/255.0 alpha:1]
#endif
