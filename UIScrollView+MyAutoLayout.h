//
//  UIView+MyAutoLayout.h
//  LDJR
//
//  Created by 郭正茂 on 15/9/30.
//  Copyright © 2015年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollContentView.h"
#import "UIView+MyAutoLayout.h"

@interface UIScrollView (GZAutoLayout)

-(ScrollContentView*)getScrollContentViewWithHeight:(CGFloat)height;
-(ScrollContentView*)getScrollContentViewWitWidth:(CGFloat)width;
-(ScrollContentView*)getScrollContentViewWitWidth:(CGFloat)width height:(CGFloat)height;
@end
