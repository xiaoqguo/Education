//
//  UIView+MyAutoLayout.m
//  LDJR
//
//  Created by 郭正茂 on 15/9/30.
//  Copyright © 2015年 ldjt. All rights reserved.
//

#import "UIView+MyAutoLayout.h"

@implementation UIView (GZAutoLayout)

#define kScreenWidth [UIApplication sharedApplication].keyWindow.frame.size.width

+(CGFloat)getScreenHeight{
    return [UIApplication sharedApplication].keyWindow.frame.size.height;
}
+(CGFloat)getScreenWidth{
//    return [UIApplication sharedApplication].keyWindow.frame.size.width;
    return  [ UIScreen mainScreen ].applicationFrame.size.width;
}
+(CGRect)getScreeFrame{
    return [UIApplication sharedApplication].keyWindow.frame;
}
#pragma mark 边线方法
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType withSideColor:(UIColor*)color withThick:(CGFloat)thick withStartDistance:(NSInteger)sDistance withEndDistance:(NSInteger)eDistance{
    UIView *line=[[UIView alloc] initWithFrame:CGRectZero];
    line.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:line];
    
    [line setBackgroundColor:color];
    NSDictionary *metrics=@{@"thick":@(thick),@"sd":@(sDistance),@"ed":@(eDistance)};
    NSDictionary *views=@{@"line":line};
    if (lineType==GZAutolayoutLineTypeTop) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-sd-[line]-ed-|" options:0 metrics:metrics views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line(thick)]" options:0 metrics:metrics views:views]];
    }else if(lineType==GZAutolayoutLineTypeRight){
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-sd-[line]-ed-|" options:0 metrics:metrics views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[line(thick)]|" options:0 metrics:metrics views:views]];
    }else if(lineType==GZAutolayoutLineTypeleft){
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-sd-[line]-ed-|" options:0 metrics:metrics views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[line(thick)]" options:0 metrics:metrics views:views]];
    }else{
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-sd-[line]-ed-|" options:0 metrics:metrics views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[line(thick)]|" options:0 metrics:metrics views:views]];
    }
    return line;
}
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType withSideColor:(UIColor*)color withThick:(CGFloat)thick withSideDistance:(NSInteger)sDistance{
    return [self addSideLineWithSideType:lineType withSideColor:color withThick:thick withStartDistance:sDistance withEndDistance:sDistance];
}
-(UIView*)addSideLineWithSideType:(GZAutolayoutLineType)lineType withSideColor:(UIColor*)color withThick:(CGFloat)thick{
    return [self addSideLineWithSideType:lineType withSideColor:color withThick:thick withStartDistance:0 withEndDistance:0];
}
#pragma mark 子试图完全填充父视图
-(void)filledWith:(UIView*)filedView{
    [self filledWith:filedView topMargin:0 bottomMargin:0 leftMargin:0 rightMargin:0];
}
-(void)filledWith:(UIView *)subView margin:(CGFloat)margin{
    [self filledWith:subView topMargin:margin bottomMargin:margin leftMargin:margin rightMargin:margin];
}
-(void)filledWith:(UIView *)subView horiMargin:(CGFloat)hm vertiMargin:(CGFloat)vm{
    [self filledWith:subView topMargin:vm bottomMargin:vm leftMargin:hm rightMargin:hm];
}
-(void)filledWith:(UIView *)subView topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm{
    NSDictionary *views=@{@"v":subView};
    NSDictionary *metrics=@{@"tm":@(tm),@"bm":@(bm),@"lm":@(lm),@"rm":@(rm)};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-lm-[v]-rm-|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-tm-[v]-bm-|" options:0 metrics:metrics views:views]];
}
#pragma mark 添加长宽固定的子视图constraint
-(void)addConstraintSubView:(UIView*)subView width:(CGFloat)width height:(CGFloat)height{
    [self addConstraint:[subView getWidthConstraintWithWidth:width]];
    [self addConstraint:[subView getHeightConstraintWithHeight:height]];
}
-(void)addConstraintWidth:(CGFloat)width height:(CGFloat)height{
    [self addConstraint:[self getWidthConstraintWithWidth:width]];
    [self addConstraint:[self getHeightConstraintWithHeight:height]];
}
#pragma mark 获得高度宽度的contraint
-(NSLayoutConstraint*)getHeightConstraintWithHeight:(CGFloat)height{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1 constant:height];
}
-(NSLayoutConstraint*)getWidthConstraintWithWidth:(CGFloat)width{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1 constant:width];
}
-(NSLayoutConstraint*)getWCompareHConstraintWithRatio:(CGFloat)ratio{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:self attribute:NSLayoutAttributeHeight
                                       multiplier:ratio constant:0];
}
#pragma mark 添加子试图与父视图的关系
#pragma mark 添加子试图与父视图水平竖直关系
-(void)addHorizontalConstraintSubView:(UIView*)subView leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin{
    NSDictionary *views=@{@"v":subView};
    NSDictionary *metrics=@{@"leftMargin":@(leftMargin),@"rightMargin":@(rightMargin)};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-leftMargin-[v]-rightMargin-|" options:0 metrics:metrics views:views]];
    
}
-(void)addVerticalConstraintSubView:(UIView*)subView topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin{
    NSDictionary *views=@{@"v":subView};
    NSDictionary *metrics=@{@"topMargin":@(topMargin),@"bottomMargin":@(bottomMargin)};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topMargin-[v]-bottomMargin-|" options:0 metrics:metrics views:views]];
}
-(void)addHorizontalConstraintSubView:(UIView*)subView margin:(CGFloat)margin{
    [self addHorizontalConstraintSubView:subView leftMargin:margin rightMargin:margin];
}
-(void)addVerticalConstraintSubView:(UIView*)subView margin:(CGFloat)margin{
    [self addVerticalConstraintSubView:subView topMargin:margin bottomMargin:margin];
}
-(void)addHorizontalContraintSubView:(UIView*)subView{
    [self addHorizontalConstraintSubView:subView margin:0];
}
-(void)addVerticalConstraintSubView:(UIView*)subView{
    [self addVerticalConstraintSubView:subView margin:0];
}
#pragma mark 子试图 居中
-(NSLayoutConstraint*)getXCenterConstraintWithSubView:(UIView*)subView withConstant:(CGFloat)constant{
    return [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:constant];
}
-(NSLayoutConstraint*)getXCenterConstraintWithSubView:(UIView*)subView{
    return [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
}
-(NSLayoutConstraint*)getYCenterConstrainWithSubView:(UIView*)subView withConstant:(CGFloat)constant{
    return [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:constant];
}
-(NSLayoutConstraint*)getYCenterConstrainWithSubView:(UIView*)subView{
    return [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
}
-(void)addCenterConstrainWithSubView:(UIView*)subView{
    [self addConstraint:[self getXCenterConstraintWithSubView:subView]];
    [self addConstraint:[self getYCenterConstrainWithSubView:subView]];
}
#pragma mark 活的子试图与self的上下左右关系
-(NSLayoutConstraint*)getTopConstraint:(CGFloat)top withSubView:(UIView*)view{
    return [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:top];
}
-(NSLayoutConstraint*)getLeftConstraint:(CGFloat)left withSubView:(UIView*)view{
     return [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:left];
}
-(NSLayoutConstraint*)getRightContraint:(CGFloat)right withSubView:(UIView*)view{
    return [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-right];
}
-(NSLayoutConstraint*)getBottomContraint:(CGFloat)bottom withSubView:(UIView*)view{
   return [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-bottom];
}
#pragma mark 子试图与子试图的关系
#pragma mark 子试图顶底相接
+(NSLayoutConstraint*)getConsTopView:(UIView*)tv bottomView:(UIView*)bv margin:(CGFloat)margin{
    return [NSLayoutConstraint constraintWithItem:tv attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:bv attribute:NSLayoutAttributeTop multiplier:1 constant:-margin];
}
+(NSLayoutConstraint*)getConsLeftView:(UIView*)lv rightView:(UIView*)rv margin:(CGFloat)margin{
    return [NSLayoutConstraint constraintWithItem:lv attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:rv attribute:NSLayoutAttributeLeading multiplier:1 constant:-margin];
}
#pragma mark 多子视图与俯视图的关系
#pragma mark 等宽、等高 挤满 父视图
-(void)addHorizontalSubViews:(NSArray*)subViews leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm viewMargin:(CGFloat)vm{
    NSString *visualFormStr=@"";
    NSMutableArray *keys=[[NSMutableArray alloc] initWithCapacity:[subViews count]];
    for(int i=0;i<[subViews count];i++){
        NSString *str=[NSString stringWithFormat:@"view%d",i];
        [keys addObject:str];
    }
    NSDictionary *views=[NSDictionary dictionaryWithObjects:subViews forKeys:keys];
    NSDictionary *metrics=@{@"lm":@(lm),@"rm":@(rm),@"vm":@(vm)};
    visualFormStr=@"|-lm-[view0]-vm-";
    for (int i=1; i<[subViews count]; i++) {
        if (i==[subViews count]-1) {
             visualFormStr=[NSString stringWithFormat:@"%@[view%d(==view0)]-rm-|",visualFormStr,i];
        }else{
             visualFormStr=[NSString stringWithFormat:@"%@[view%d(==view0)]-vm-",visualFormStr,i];
        }
    }
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:visualFormStr options:0 metrics:metrics views:views]];
}
-(void)addVerticalSubViews:(NSArray*)subViews topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm viewMargin:(CGFloat)vm{
    
}

-(void)addHorizontalChainSubViews:(NSArray*)subViews chaintype:(GZAutoLayoutVeriticalChaintype)type constant:(CGFloat)constant margin:(CGFloat)margin{
   
    
}
-(void)addVerticalChainSubViews:(NSArray*)subViews chaintype:(GZAutoLayoutVeriticalChaintype)type constant:(CGFloat)constant margin:(CGFloat)margin{
    __block UIView *lastView=nil;
    [subViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *view=(UIView*)obj;
        if (lastView!=nil) {
            [self addConstraint:[UIView getConsTopView:lastView bottomView:view margin:margin]];
        }
        if (type==GZAutolayoutVeriticalChainLeft) {
            [self addConstraint:[self getLeftConstraint:constant withSubView:view]];
        }else if(type==GZAutolayoutVeriticalChainRight){
            [self addConstraint:[self getRightContraint:constant withSubView:view]];
        }else{
            [self addConstraint:[self getXCenterConstraintWithSubView:view withConstant:constant]];
        }
        lastView=view;
    }];
}
-(NSLayoutConstraint*)getTopEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:bv attribute:NSLayoutAttributeTop multiplier:1 constant:contant];
}
-(NSLayoutConstraint*)getBottomEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:bv attribute:NSLayoutAttributeBottom multiplier:1 constant:contant];
}
-(NSLayoutConstraint*)getLeftEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:bv attribute:NSLayoutAttributeLeft multiplier:1 constant:contant];
}
-(NSLayoutConstraint*)getRightEquelConstraintToView:(UIView*)bv withContant:(CGFloat)contant{
     return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:bv attribute:NSLayoutAttributeRight multiplier:1 constant:contant];
}
-(void)addContraintsSubView:(UIView*)sView underView:(UIView*)uView constant:(CGFloat)constant{
    [self addConstraint:[UIView getConsEqualWithAView:sView bView:uView withRadio:1]];
    [self addConstraint:[sView getLeftEquelConstraintToView:uView withContant:0]];
    [self addConstraint:[UIView getConsTopView:uView bottomView:sView margin:constant]];
}
-(void)addContraintsSubView:(UIView*)sView underView:(UIView*)uView constant:(CGFloat)constant height:(CGFloat)height{
    [self addContraintsSubView:sView underView:uView constant:constant];
    [sView addConstraint:[sView getHeightConstraintWithHeight:height]];
}
#pragma mark 两视图宽比例 和高比例
+(NSLayoutConstraint*)getConsEqualWithAView:(UIView*)aView bView:(UIView*)bView  withRadio:(CGFloat)radio{
    return [NSLayoutConstraint constraintWithItem:aView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:bView attribute:NSLayoutAttributeWidth multiplier:radio constant:0];
}
+(NSLayoutConstraint*)getConsEqualHeightAView:(UIView*)aView bView:(UIView*)bView  withRadio:(CGFloat)radio{
    return [NSLayoutConstraint constraintWithItem:aView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:bView attribute:NSLayoutAttributeHeight multiplier:radio constant:0];
}

-(NSLayoutConstraint*)getWidthEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple{
     return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeWidth multiplier:mutiple constant:0];
}
-(NSLayoutConstraint*)getHeightEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeHeight multiplier:mutiple constant:0];
}
-(NSLayoutConstraint*)getXCenterEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeCenterX multiplier:mutiple constant:0];
}
-(NSLayoutConstraint*)geXCenterEqualConsToView:(UIView*)toView constant:(CGFloat)constant{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeCenterX multiplier:1 constant:constant];
}
-(NSLayoutConstraint*)getXCenterEqualConsToView:(UIView*)toView{
    return [self geXCenterEqualConsToView:toView constant:0];
}
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView constant:(CGFloat)constant{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeCenterY multiplier:1 constant:constant];
}
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView{
     return [self getYCenterEqualConsToView:toView constant:0];
}
-(NSLayoutConstraint*)getYCenterEqualConsToView:(UIView*)toView mutiple:(CGFloat)mutiple{
    return [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:toView attribute:NSLayoutAttributeCenterY multiplier:mutiple constant:0];
}
#pragma mark 自身与其他视图关系

//父视图方法 上下左右对齐
-(void)addConsSubView:(UIView*)sv sameLayoutToView:(UIView*)view{
    [self addConstraint:[sv getTopEquelConstraintToView:view withContant:0]];
    [self addConstraint:[sv getBottomEquelConstraintToView:view withContant:0]];
    [self addConstraint:[sv getLeftEquelConstraintToView:view withContant:0]];
    [self addConstraint:[sv getRightEquelConstraintToView:view withContant:0]];
}
-(void)addConsSubView:(UIView *)sv licklyLayoutToView:(UIView *)view topCon:(CGFloat)top bottomCon:(CGFloat)bottom leftCon:(CGFloat)left rightCon:(CGFloat)right{
    [self addConstraint:[sv getTopEquelConstraintToView:view withContant:top]];
    [self addConstraint:[sv getBottomEquelConstraintToView:view withContant:bottom]];
    [self addConstraint:[sv getLeftEquelConstraintToView:view withContant:left]];
    [self addConstraint:[sv getRightEquelConstraintToView:view withContant:right]];
}
#pragma mark 添加图片
-(UIImageView*)addHoriImgViewWithName:(NSString*)name{
    UIImage *img=[UIImage imageNamed:name];
    UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectZero];
    imgView.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:imgView];
    if (img==nil) {
        return imgView;
    }
    [imgView setImage:img];
    [self addHorizontalContraintSubView:imgView];
    [self addConstraint:[imgView getWCompareHConstraintWithRatio:img.size.width/img.size.height]];
    return imgView;
}
-(UIImageView*)addCenterImageWithImage:(UIImage*)img sacle:(CGFloat)sacle{
    UIImageView *imgView=[self addImageConsWithImg:img sacle:sacle];
    [self addCenterConstrainWithSubView:imgView];
    
    return imgView;
}
-(UIImageView*)addImageConsWithImg:(UIImage*)img sacle:(CGFloat)sacle{
    CGFloat width=img.size.width*sacle;
    CGFloat height=img.size.height*sacle;
    UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectZero];
    imgView.translatesAutoresizingMaskIntoConstraints=NO;
    [imgView setImage:img];
    [self addSubview:imgView];
    [self addConstraintSubView:imgView width:width height:height];
    return imgView;
}
-(UIButton*)addCenterBTWithImage:(UIImage*)omg sacle:(CGFloat)sacle{
    UIButton *bt=[self addBTWithImage:omg sacle:sacle];
    [self addConstraint:[self getXCenterConstraintWithSubView:bt]];
    return bt;
}
-(UIButton*)addBTWithImage:(UIImage*)img sacle:(CGFloat)sacle{
    CGFloat width=img.size.width*sacle;
    CGFloat height=img.size.height*sacle;
    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
    bt.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:bt];
    [bt setImage:img forState:UIControlStateNormal];
    [self addConstraintSubView:bt width:width height:height];
    return bt;
}
#pragma mark scrollView相关

-(NSLayoutConstraint*)addToScrollView:(UIScrollView*)view withHeight:(CGFloat)height{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualWithAView:self bView:view withRadio:1]];
    NSLayoutConstraint *heightConstraint=[self getHeightConstraintWithHeight:height];
    [view addConstraint:heightConstraint];
    [view filledWith:self];
    return heightConstraint;
}
-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withWidth:(CGFloat)width{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualHeightAView:self bView:view withRadio:1]];
    NSLayoutConstraint *widthConstraint=[self getWidthConstraintWithWidth:width];
    [view addConstraint:widthConstraint];
    [view filledWith:self];
    return widthConstraint;
}
-(void)addToScrollView:(UIScrollView*)view withHeightMutiple:(CGFloat)mutiple{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualWithAView:self bView:view withRadio:1]];
    NSLayoutConstraint *heightConstraint=[UIView getConsEqualHeightAView:self bView:view withRadio:mutiple];
    [view addConstraint:heightConstraint];
    [view filledWith:self];
}
-(void)addToScrollView:(UIScrollView*)view withWidthMutiple:(CGFloat)mutiple{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualHeightAView:self bView:view withRadio:1]];
    NSLayoutConstraint *widthConstraint=[UIView getConsEqualWithAView:self bView:view withRadio:mutiple];
    [view addConstraint:widthConstraint];
    [view filledWith:self];
}
-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withHeightMultiplier:(CGFloat)multiplier{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualWithAView:self bView:view withRadio:1]];
    NSLayoutConstraint *heightConstraint=[UIView getConsEqualHeightAView:self bView:view withRadio:multiplier];
    [view addConstraint:heightConstraint];
    [view filledWith:self];
    return heightConstraint;
}

-(NSLayoutConstraint*)addToScrollView:(UIScrollView *)view withWidthMultiplier:(CGFloat)multiplier{
    self.translatesAutoresizingMaskIntoConstraints=NO;
    [view addSubview:self];
    [view addConstraint:[UIView getConsEqualHeightAView:self bView:view withRadio:1]];
    NSLayoutConstraint *widthConstraint=[UIView getConsEqualWithAView:self bView:view withRadio:multiplier];
    [view addConstraint:widthConstraint];
    [view filledWith:self];
    return widthConstraint;
}
@end
