//
//  MainViewController.m
//  Education
//
//  Created by 郭正茂 on 16/10/31.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "MainViewController.h"
#import "UIView+MyAutoLayout.h"
#import "MainListCell.h"
#import "TrainStatNode.h"
#import "ViewController+Base.h"
#import "BaseColor.h"
#import "MainTitleView.h"
#import "MainLeftView.h"
@interface MainViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSLayoutConstraint *mainLeftCons;
@property(nonatomic,strong)UIView *titlePanel;
@property(nonatomic,strong)UITableView *listTV;
@property(nonatomic,strong)NSMutableArray *array;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftViewWidth:300];
    [self initLeftView];
    [self.leftView setBackgroundColor:[UIColor whiteColor]];
    [self initData];
    [self initTitlePanel];
    [self initListPanel];
    // Do any additional setup after loading the view.
}
-(void)showSignoutAction{
//    [self dissLeftDrawView];
     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"确定要注销吗？" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }]];
     [self presentViewController:alertController animated:YES completion:nil];
}
-(void)initData{
    self.array=[[NSMutableArray alloc] initWithCapacity:4];
    {
        TrainStatNode *node=[[TrainStatNode alloc] init];
        node.name=@"初三第二课 语法中级训练";
        node.total=@"10";
        node.finishCount=@"0";
        node.state=@"未完成";
        node.stateNum=0;
        [self.array addObject:node];
    }
    {
        TrainStatNode *node=[[TrainStatNode alloc] init];
        node.name=@"初三第二课 听力初级训练";
        node.total=@"10";
        node.finishCount=@"10";
        node.state=@"已完成";
        node.stateNum=1;
        [self.array addObject:node];
    }
    {
        TrainStatNode *node=[[TrainStatNode alloc] init];
        node.name=@"初三第二课 口语初级训练";
        node.total=@"2";
        node.finishCount=@"1";
        node.state=@"未完成";
        node.stateNum=0;
        [self.array addObject:node];
    }
    {
        TrainStatNode *node=[[TrainStatNode alloc] init];
        node.name=@"初三第二课 翻译中级训练";
        node.total=@"1";
        node.finishCount=@"0";
        node.state=@"未完成";
        node.stateNum=0;
        [self.array addObject:node];
    }
}
-(void)initLeftView{
    MainLeftView *view=[[MainLeftView alloc] initWithFrame:CGRectZero];
    view.translatesAutoresizingMaskIntoConstraints=NO;
    [self.leftView addSubview:view];
    [self.leftView filledWith:view];
    __weak MainViewController *weakSelf=self;
    view.sigoutBlock=^(){
        [weakSelf showSignoutAction];
    };
}
-(void)initTitlePanel{
    MainTitleView *view=[[MainTitleView alloc] initWithSuperView:self.mainView];
    [view setTitle:@"我的课程练习"];
    [view setTotalStr:@"23"];
    [view setFinishStr:@"11"];
    __weak MainViewController *weakSelf=self;
    view.showLeftDrawBlock=^(){
        [weakSelf showLeftDrawView];
    };
    self.titlePanel=view;
}

-(void)initListPanel{
    UITableView *view=[[UITableView alloc] initWithFrame:CGRectZero];
    view.translatesAutoresizingMaskIntoConstraints=NO;
    [self.mainView addSubview:view];
    [view setBackgroundColor:white_color];
    
    [self.mainView addHorizontalContraintSubView:view];
    [self.mainView addConstraint:[UIView getConsTopView:self.titlePanel bottomView:view margin:0]];
    [self.mainView addConstraint:[self.mainView getBottomContraint:0 withSubView:view]];
    
    view.delegate=self;
    view.dataSource=self;
    self.listTV=view;
}
-(void)addMainCellText:(NSString*)text desc:(NSString*)desc{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.array count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"main_list_cell"];
    if (cell==nil) {
        cell=[[MainListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"main_list_cell"];
    }
    [cell setNode:[self.array objectAtIndex:indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showLeftDrawView];
}
@end
