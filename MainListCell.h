//
//  MainListCell.h
//  Education
//
//  Created by 郭正茂 on 16/10/31.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainStatNode.h"
@interface MainListCell : UITableViewCell
-(void)setNode:(TrainStatNode*)node;
@end
