//
//  MainTitleView.m
//  Education
//
//  Created by 郭正茂 on 2016/11/3.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "MainTitleView.h"
#import "UIView+MyAutoLayout.h"
#import "BaseColor.h"
@interface MainTitleView()
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UILabel *totalLb;
@property(nonatomic,strong)UILabel *finishLb;
@end
@implementation MainTitleView
-(id)initWithSuperView:(UIView*)superView{
    self=[self initWithFrame:CGRectZero];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints=NO;
        [self addToSuperView:superView];
        [self setBackgroundColor:main_normal_color];
        [self initTitleLabel];
        [self initLeftBT];
        [self initLbLine];
    }
    return self;
}
-(void)addToSuperView:(UIView*)superView{
    [superView addSubview:self];
    [superView addHorizontalContraintSubView:self];
    [superView addConstraint:[superView getTopConstraint:0 withSubView:self]];
    [self addConstraint:[self getHeightConstraintWithHeight:108]];
}
-(void)setTitle:(NSString*)title{
    self.titleLb.text=title;
}
-(void)setTotalStr:(NSString*)total{
    [self.totalLb setText:[NSString stringWithFormat:@"总练习：%@",total]];
}
-(void)setFinishStr:(NSString*)finish{
   [self.finishLb setText:[NSString stringWithFormat:@"已完成：%@",finish]];
}
-(void)initTitleLabel{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:lb];
    
    [self addHorizontalContraintSubView:lb];
    [self addConstraint:[self getTopConstraint:20 withSubView:lb]];
    [lb addConstraint:[lb getHeightConstraintWithHeight:44]];
    
    [lb setTextAlignment:NSTextAlignmentCenter];
    [lb setFont:[UIFont systemFontOfSize:18]];
    [lb setTextColor:white_color];
    
    self.titleLb=lb;
}
-(void)initLbLine{
    UILabel *tlb=[[UILabel alloc] initWithFrame:CGRectZero];
    UILabel *flb=[[UILabel alloc] initWithFrame:CGRectZero];
    UIView *line=[[UIView alloc] initWithFrame:CGRectZero];
    line.translatesAutoresizingMaskIntoConstraints=NO;
    tlb.translatesAutoresizingMaskIntoConstraints=NO;
    flb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:line];
    
    [self addHorizontalContraintSubView:line];
    [line addConstraint:[line getHeightConstraintWithHeight:40]];
    [self addConstraint:[self getBottomContraint:0 withSubView:line]];
    
    
    [line addSubview:tlb];
    [line addSubview:flb];
    [line addHorizontalSubViews:@[tlb,flb] leftMargin:0 rightMargin:0 viewMargin:0];
    [line addVerticalConstraintSubView:tlb];
    [line addVerticalConstraintSubView:flb];
    [self setLabel:tlb];
    [self setLabel:flb];
    self.totalLb=tlb;
    self.finishLb=flb;
}
-(void)setLabel:(UILabel*)lb{
    [lb setTextAlignment:NSTextAlignmentCenter];
    [lb setTextColor:main_light_color];
    [lb setFont:[UIFont systemFontOfSize:14]];
}
-(void)initLeftBT{
    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
    bt.frame=CGRectMake(10, 22, 32, 32);
//    [bt setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:bt];
    [bt addTarget:self action:@selector(leftBTClickAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitle:@"m" forState:UIControlStateNormal];
}
-(void)leftBTClickAction{
    if(self.showLeftDrawBlock){
        self.showLeftDrawBlock();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
