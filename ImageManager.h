//
//  ImageManager.h
//  GMSupperWH
//
//  Created by apple on 14-10-24.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ImageManager : NSObject

+(CGSize)getImageSizeWithWidth:(NSInteger)width withImageSize:(CGSize)imgSize;
+(CGRect)getFrameWithImageSize:(CGSize)imgSize withContentSize:(CGSize)contentSize;
+(NSArray*)getNameAndTypeWithFileName:(NSString*)fileName;
+(UIImage*)compressImage:(UIImage*)img forSize:(CGSize)size;
@end
