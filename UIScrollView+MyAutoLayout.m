//
//  UIView+MyAutoLayout.m
//  LDJR
//
//  Created by 郭正茂 on 15/9/30.
//  Copyright © 2015年 ldjt. All rights reserved.
//

#import "UIView+MyAutoLayout.h"
#import "UIScrollView+MyAutoLayout.h"
@implementation UIScrollView (GZAutoLayout)

-(ScrollContentView*)getScrollContentViewWithHeight:(CGFloat)height{
    ScrollContentView *scView=[[ScrollContentView alloc] initWithFrame:CGRectZero];
    scView.heightConstraint=[scView addToScrollView:self withHeight:height];
    return scView;
}
-(ScrollContentView*)getScrollContentViewWitWidth:(CGFloat)width{
    ScrollContentView *scView=[[ScrollContentView alloc] initWithFrame:CGRectZero];
    scView.widthConstraint=[scView addToScrollView:self withWidth:width];
    return scView;
}
-(ScrollContentView*)getScrollContentViewWitWidth:(CGFloat)width height:(CGFloat)height{
    ScrollContentView *scView=[[ScrollContentView alloc] initWithFrame:CGRectZero];
    scView.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:scView];
    [self filledWith:scView];
    scView.widthConstraint=[self getWidthConstraintWithWidth:width];
    scView.heightConstraint=[self getHeightConstraintWithHeight:height];
    [self addConstraint:scView.widthConstraint];
    [self addConstraint:scView.heightConstraint];
    return scView;
}
@end
