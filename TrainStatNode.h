//
//  TrainStatNode.h
//  Education
//
//  Created by 郭正茂 on 16/10/31.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrainStatNode : NSObject
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *total;
@property(nonatomic,strong)NSString *finishCount;
@property(nonatomic,strong)NSString *state;
@property(nonatomic,assign)int stateNum;//0 未完成 1已完成
@end
