//
//  AppDelegate.h
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

