//
//  main.m
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
