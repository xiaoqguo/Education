//
//  ViewController.m
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "ViewController.h"
#import "BaseColor.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:main_normal_color];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
