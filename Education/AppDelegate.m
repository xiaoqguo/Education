//
//  AppDelegate.m
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#import "RLDrawViewController.h"
@interface AppDelegate ()
@property(nonatomic,strong)UINavigationController *rootNavVC;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.rootNavVC=[[UINavigationController alloc] init];
    self.rootNavVC.navigationBar.hidden=YES;
    self.window.rootViewController=self.rootNavVC;
//    [self text];
//    [self showMainVC];
    [self showLoginVC];
    return YES;
}
-(void)text{
//    RLDrawViewController *vc=[[RLDrawViewController alloc] init];
//    [vc setRightViewWidth:180];
//    [self.rootNavVC pushViewController:vc animated:YES];
//    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
//    bt.frame=CGRectMake(100, 100, 80, 30);
//    [vc.view addSubview:bt];
//    
////    [bt addTarget:self action:@selector(test2) forControlEvents:UIControlEventTouchUpInside];
//    [bt addTarget:vc action:@selector(showRightDrawView) forControlEvents:UIControlEventTouchUpInside];
//    [vc.view addSubview:bt];
//    [bt setBackgroundColor:[UIColor whiteColor]];
//    
//    [vc.mainView setBackgroundColor:[UIColor orangeColor]];
//    [vc.rightView setBackgroundColor:[UIColor blueColor]];
//    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
//    [view setBackgroundColor:[UIColor redColor]];
//    [vc.rightView addSubview:view];
    
    
}
-(void)test2{
    NSLog(@"test2");
}
-(void)showLoginVC{
    LoginViewController *loginVC=[[LoginViewController alloc] init];
    [self.rootNavVC pushViewController:loginVC animated:NO];
    loginVC.teacherLoginBlock=^(){
        
    };
    loginVC.studentLoginBlock=^(){
        [self showMainVC];
    };
}
-(void)showMainVC{
    MainViewController *mainVC=[[MainViewController alloc] init];
    [self.rootNavVC pushViewController:mainVC animated:YES];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
