//
//  RLDrawViewController.h
//  Education
//
//  Created by 郭正茂 on 2016/11/2.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLDrawViewController : UIViewController
@property(nonatomic,strong)UIView *mainView;
@property(nonatomic,strong)UIView *coverView;
@property(nonatomic,strong)UIView *leftView;
@property(nonatomic,strong)UIView *rightView;

-(void)setLeftViewWidth:(CGFloat)width;
-(void)showLeftDrawView;
-(void)setRightViewWidth:(CGFloat)width;
-(void)showRightDrawView;
-(void)dissDrawAction;
-(void)dissLeftDrawView;
-(void)dissRightDrawView;
@end
