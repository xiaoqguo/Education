//
//  ViewController.m
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "ViewController+Base.h"

@interface UIViewController ()

@end

@implementation UIViewController (Base)
-(void)addDissmissKeybordTap{
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmissKeybordTapAction)];
    [self.view addGestureRecognizer:tap];
}
-(void)dissmissKeybordTapAction{
    [self.view endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
