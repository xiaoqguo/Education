//
//  MainLeftView.m
//  Education
//
//  Created by 郭正茂 on 2016/11/4.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "MainLeftView.h"
#import "UIView+MyAutoLayout.h"
#import "BaseColor.h"
@interface MainLeftView()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *textArray;
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UIButton *bottomBT;
@property(nonatomic,strong)UITableView *tableView;
@end
@implementation MainLeftView
-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.textArray=@[@"姓名：周杰伦",@"班级：三年二班",@"学号：9617",@"总练习次数：224"];
        [self addSideLineWithSideType:GZAutolayoutLineTypeTop withSideColor:main_normal_color withThick:20];
        [self initTitle];
        [self initBottomBT];
        [self initTableView];
    }
    return self;
}

-(void)initTitle{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:lb];
    
    [self addHorizontalContraintSubView:lb];
    [lb addConstraint:[lb getHeightConstraintWithHeight:44]];
    [self addConstraint:[self getTopConstraint:20 withSubView:lb]];
    
    [lb setBackgroundColor:main_normal_color];
    [lb setTextColor:white_color];
    [lb setTextAlignment:NSTextAlignmentCenter];
    [lb setFont:[UIFont systemFontOfSize:18]];
    
    [lb setText:@"个人信息"];
    
    self.titleLb=lb;
}
-(void)initBottomBT{
    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
    bt.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:bt];
    
    [self addHorizontalConstraintSubView:bt margin:60];
    [self addConstraint:[self getBottomContraint:10 withSubView:bt]];
    [bt addConstraint:[bt getHeightConstraintWithHeight:40]];
    
    [bt setBackgroundColor:error_color];
    [bt setTitleColor:white_color forState:UIControlStateNormal];
    [bt setTitleColor:main_light_color forState:UIControlStateHighlighted];
    [bt setTitle:@"注销" forState:UIControlStateNormal];
    bt.layer.cornerRadius=20;
    
    [bt addTarget:self action:@selector(signoutAction) forControlEvents:UIControlEventTouchUpInside];
    self.bottomBT=bt;
}
-(void)initTableView{
    UITableView *tableView=[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:tableView];

    tableView.delegate=self;
    tableView.dataSource=self;
    
    [self addHorizontalContraintSubView:tableView];
    [self addConstraint:[UIView getConsTopView:self.titleLb bottomView:tableView margin:0]];
    [self addConstraint:[UIView getConsTopView:tableView bottomView:self.bottomBT margin:0]];
    
    self.tableView=tableView;
    
}
-(void)signoutAction{
    if (self.sigoutBlock) {
        self.sigoutBlock();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.textArray count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"main_left_cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"main_left_cell"];
    }
    [cell.textLabel setText:[self.textArray objectAtIndex:indexPath.row]];
    return cell;
}

@end
