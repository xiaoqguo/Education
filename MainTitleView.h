//
//  MainTitleView.h
//  Education
//
//  Created by 郭正茂 on 2016/11/3.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTitleView : UIView
@property(nonatomic,copy)void (^showLeftDrawBlock)();
-(id)initWithSuperView:(UIView*)superView;
-(void)setTitle:(NSString*)title;
-(void)setTotalStr:(NSString*)total;
-(void)setFinishStr:(NSString*)finish;
@end
