//
//  MainListCell.m
//  Education
//
//  Created by 郭正茂 on 16/10/31.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "MainListCell.h"
#import "UIView+MyAutoLayout.h"
#import "BaseColor.h"
@interface MainListCell()
@property(nonatomic,strong)UILabel *nameLb;
@property(nonatomic,strong)UILabel *progressLb;
@property(nonatomic,strong)UILabel *stateLb;
@end
@implementation MainListCell
-(void)initNameLb{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:lb];
    
    [self addConstraint:[self getYCenterConstrainWithSubView:lb]];
    [self addConstraint:[self getLeftConstraint:20 withSubView:lb]];
    [lb addConstraintWidth:240 height:20];
    
    [lb setTextColor:text_primary_color];
    self.nameLb=lb;
}
-(void)initProgressLb{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:lb];
    
    [self addConstraint:[self getRightContraint:20 withSubView:lb]];
    [self addConstraint:[self getBottomContraint:6 withSubView:lb]];
    [lb addConstraintWidth:150 height:14];
    
    [lb setTextAlignment:NSTextAlignmentRight];
    [lb setFont:[UIFont systemFontOfSize:12]];
    [lb setTextColor:text_secondary_color];
    self.progressLb=lb;
    
}
-(void)initStateLb{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self addSubview:lb];
    
    [self addConstraint:[self getYCenterConstrainWithSubView:lb]];
    [self addConstraint:[self getRightContraint:20 withSubView:lb]];
    [lb addConstraintWidth:80 height:20];
    
    [lb setFont:[UIFont systemFontOfSize:14]];
    [lb setTextAlignment:NSTextAlignmentRight];
//    [lb setTextColor:tex]
    self.stateLb=lb;
}
-(void)setNode:(TrainStatNode*)node{
    if (self.nameLb==nil) {
        [self initNameLb];
    }
    [self.nameLb setText:node.name];
    if(self.stateLb==nil){
        [self initStateLb];
    }
    [self.stateLb setText:node.state];
    if (node.stateNum==0) {
        [self.stateLb setTextColor:error_color];
    }else{
        [self.stateLb setTextColor:success_color];
    }
    if (self.progressLb==nil) {
        [self initProgressLb];
    }
    [self.progressLb setText:[NSString stringWithFormat:@"(%@/%@)",node.finishCount,node.total]];
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
