//
//  LoginViewController.m
//  Education
//
//  Created by 郭正茂 on 16/10/28.
//  Copyright © 2016年 ldjt. All rights reserved.
//

#import "LoginViewController.h"
#import "BaseColor.h"
#import "UIView+MyAutoLayout.h"
#import "ViewController+Base.h"
#import "GZTabScrollView.h"
@interface LoginViewController ()
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UILabel *descLb;
@property(nonatomic,strong)GZTabScrollView *roleSelectSV;
@property(nonatomic,strong)UITextField *userNameTF;
@property(nonatomic,strong)UITextField *pwdTF;
@property(nonatomic,strong)UIButton *forgetPwdBT;
@property(nonatomic,strong)UILabel *selectedTabLb;
@property(nonatomic,strong)UIButton *submitBT;
@property(nonatomic,assign)NSInteger loginType;//0 教师 1学生
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:main_normal_color];
    self.loginType=0;
    [self addTitleBG];
    [self initRoleSelectSV];
    [self initTitleLb];
    [self initDescLb];
    [self initUserNameTF];
    [self initPWDTF];
    [self initForgetPwdBt];
    [self initSubmitBT];
    [self addDissmissKeybordTap];
    // Do any additional setup after loading the view.
}
-(void)loginAction{
    if(self.loginType==0){
        if(self.teacherLoginBlock){
            self.teacherLoginBlock();
        }
    }else{
        if(self.studentLoginBlock){
            self.studentLoginBlock();
        }
    }
}
-(void)addTitleBG{
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    view.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:view];
    
    [self.view addHorizontalContraintSubView:view];
    [self.view addConstraint:[self.view getTopConstraint:0 withSubView:view]];
    [view addConstraint:[view getHeightConstraintWithHeight:260]];
    
    [view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    
}
-(void)initTitleLb{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:lb];
    
    [self.view addHorizontalContraintSubView:lb];
    [lb addConstraint:[lb getHeightConstraintWithHeight:30]];
    [self.view addConstraint:[self.view getTopConstraint:120 withSubView:lb]];
    
    [lb setFont:[UIFont systemFontOfSize:28]];
    [lb setTextColor:white_color];
    [lb setTextAlignment:NSTextAlignmentCenter];
    
//    [lb setText:@"北海中学英语测评系统"];
    [lb setText:@"绿地吉客网拍卖系统"];
    self.titleLb=lb;
}
-(void)initRoleSelectSV{
    __block NSMutableArray *lbArray=[[NSMutableArray alloc] initWithCapacity:2];
    NSMutableArray *viewArray=[[NSMutableArray alloc] initWithCapacity:2];
    GZTabScrollView *tabView=[[GZTabScrollView alloc] initWithFrame:CGRectMake(0,220, [UIView getScreenWidth], 40)];
    [self.view addSubview:tabView];
    [tabView setCusorColor:green_color withCusorLineColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]];
    [tabView setTabItemNormalColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]];
    [tabView setTabItemSelectedColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]];
    [tabView setCusorType:1];
    CGSize tabSize=[tabView getTabBarItemSizeWithPageCount:2];
    for (int i=0; i<2; i++) {
        UIView *subView=[[UIView alloc] initWithFrame:CGRectZero];
        [viewArray addObject:subView];
        UILabel *lb=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, tabSize.width, tabSize.height)];
        [lbArray addObject:lb];
        
        lb.textAlignment=NSTextAlignmentCenter;
        [lb setFont:[UIFont systemFontOfSize:12]];
        if (i==0) {
            self.selectedTabLb=lb;
            [lb setText:@"我是老师"];
            [lb setTextColor:white_color];
        }else if(i==1){
            [lb setTextColor:main_light_color];
            [lb setText:@"我是学生"];
        }
    }
    [tabView buildWithViewArray:lbArray withSubViewArray:viewArray];
    [tabView removePageControl];
    [self.view addSubview:tabView];
    tabView.didSelectTabBarItemBlock=^(GZTabScrollView *tsView ,NSInteger pageindex){
        [self.selectedTabLb setTextColor:main_light_color];
        UILabel *lb=[lbArray objectAtIndex:pageindex];
        [lb setTextColor:white_color];
        self.selectedTabLb=lb;
        self.loginType=pageindex;
    };
    self.roleSelectSV=tabView;
}
-(void)initDescLb{
    UILabel *lb=[[UILabel alloc] initWithFrame:CGRectZero];
    lb.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:lb];
    
    [self.view addHorizontalContraintSubView:lb];
    [lb addConstraint:[lb getHeightConstraintWithHeight:20]];
    [self.view addConstraint:[UIView getConsTopView:self.titleLb bottomView:lb margin:10]];
    
    [lb setTextColor:main_light_color];
    [lb setTextAlignment:NSTextAlignmentCenter];
    
    [lb setText:@"绿地集团世界500强"];
    self.descLb=lb;
}
-(void)initUserNameTF{
    UITextField *tf=[[UITextField alloc] initWithFrame:CGRectZero];
    tf.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:tf];
    
    [self.view addHorizontalConstraintSubView:tf margin:20];
    [tf addConstraint:[tf getHeightConstraintWithHeight:48]];
    [self.view addConstraint:[UIView getConsTopView:self.descLb bottomView:tf margin:80]];
    
    [tf setPlaceholder:@"用户名"];
    [tf setTextColor:white_color];
    [tf setValue:main_light_color forKeyPath:@"_placeholderLabel.textColor"];
//    [tf addSideLineWithSideType:GZAutolayoutLineTypeTop withSideColor:divider_color withThick:0.5];
    [tf addSideLineWithSideType:GZAutolayoutLineTypeBottom withSideColor:divider_color withThick:0.5];
    self.userNameTF=tf;
}
-(void)initPWDTF{
    UITextField *tf=[[UITextField alloc] initWithFrame:CGRectZero];
    tf.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:tf];
    
    [self.view addHorizontalConstraintSubView:tf margin:20];
    [tf addConstraint:[tf getHeightConstraintWithHeight:48]];
    [self.view addConstraint:[UIView getConsTopView:self.userNameTF bottomView:tf margin:0]];
    
    
    [tf setPlaceholder:@"密码"];
    [tf setTextColor:white_color];
    [tf setValue:main_light_color forKeyPath:@"_placeholderLabel.textColor"];
    [tf addSideLineWithSideType:GZAutolayoutLineTypeBottom withSideColor:divider_color withThick:0.5];
    self.pwdTF=tf;
}
-(void)initForgetPwdBt{
    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
    bt.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:bt];
    
    bt.titleLabel.font=[UIFont systemFontOfSize:10];
    [bt addConstraintWidth:100 height:30];
    [self.view addConstraint:[UIView getConsTopView:self.pwdTF bottomView:bt margin:4]];
    [self.view addConstraint:[self.view getRightContraint:20 withSubView:bt]];
    
    [bt setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [bt setTitleColor:main_light_color forState:UIControlStateNormal];
}
-(void)initSubmitBT{
    UIButton *bt=[UIButton buttonWithType:UIButtonTypeCustom];
    bt.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:bt];
    
    [self.view addHorizontalConstraintSubView:bt margin:60];
    [self.view addConstraint:[UIView getConsTopView:self.pwdTF bottomView:bt margin:70]];
    [bt addConstraint:[bt getHeightConstraintWithHeight:40]];
    
    [bt setBackgroundColor:green_color];
    [bt setTitleColor:main_light_color forState:UIControlStateNormal];
    [bt setTitleColor:white_color forState:UIControlStateHighlighted];
    [bt setTitle:@"登录" forState:UIControlStateNormal];
    bt.layer.cornerRadius=20;
    
    [bt addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    self.submitBT=bt;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
